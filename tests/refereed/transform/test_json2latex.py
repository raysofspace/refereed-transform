import json
import os.path
from pathlib import Path

from refereed.transform import (
    abstract,
    figure_equation,
    figure_table,
    inline,
    json2latex,
    latex_inline,
    reference,
    text,
)

path_to_current_file = os.path.realpath(__file__)
current_directory = os.path.split(path_to_current_file)[0]


def test_text():
    text_json = {"text": "Text of the paper", "type": "text"}
    text_latex = text(text_json)
    test_text_latex = "Text of the paper"
    assert test_text_latex == text_latex


def test_bold_text():
    text_json = {"text": "Bold text", "type": "text", "marks": [{"type": "bold"}]}
    text_latex = text(text_json)
    test_text_latex = "\\textbf{Bold text}"
    assert test_text_latex == text_latex


def test_italic_text():
    text_json = {"text": "Italic text", "type": "text", "marks": [{"type": "italic"}]}
    text_latex = text(text_json)
    test_text_latex = "\\textit{Italic text}"
    assert test_text_latex == text_latex


def test_bold_italic_text():
    text_json = {
        "text": "Italic text",
        "type": "text",
        "marks": [{"type": "bold"}, {"type": "italic"}],
    }
    text_latex = text(text_json)
    test_text_latex = "\\textit{\\textbf{Italic text}}"
    assert test_text_latex == text_latex


# def test_reference():
#     reference_json = {
#         "type": "reference",
#         "attrs": {"data-id": "123", "data-number": "1"},
#     }
#     reference_latex = reference(reference_json)
#     test_reference_latex = "\\ref{123}"
#     assert test_reference_latex == reference_latex


def test_latex_inline():
    latex_inline_json = {"type": "latex_inline", "attrs": {"data-latex": "x^2+y^2"}}
    latex_inline_latex = latex_inline(latex_inline_json)
    test_latex_inline_latex = "$x^2+y^2$"
    assert test_latex_inline_latex == latex_inline_latex


# def test_figure_equation():
#     figure_equation_json = {
#         "type": "figure_equation",
#         "attrs": {"data-id": "123456789"},
#         "content": [{"type": "latex_block", "attrs": {"data-latex": "E=mc^2"}}],
#     }
#     figure_equation_latex = figure_equation(figure_equation_json)
#     test_figure_equation_latex = "\\begin{equation} \\label{123456789}\nE=mc^2\n\\end{equation}"
#     assert test_figure_equation_latex == figure_equation_latex


# def test_figure_table():
#     figure_table_json = {
#         "type": "figure_table",
#         "attrs": {"data-id": "83aa4fee-6879-439b-8601-74e25bb3026e"},
#         "content": [
#             {"type": "figure_caption", "content": [{"type": "text", "text": "Tabular data"}]},
#             {
#                 "type": "table",
#                 "content": [
#                     {
#                         "type": "table_row",
#                         "content": [
#                             {
#                                 "type": "table_cell",
#                                 "attrs": {"colspan": 1, "rowspan": 1},
#                                 "content": [{"type": "paragraph", "content": [{"type": "text", "text": "1"}]}],
#                             },
#                             {
#                                 "type": "table_cell",
#                                 "attrs": {"colspan": 1, "rowspan": 1},
#                                 "content": [{"type": "paragraph", "content": [{"type": "text", "text": "2"}]}],
#                             },
#                             {
#                                 "type": "table_cell",
#                                 "attrs": {"colspan": 1, "rowspan": 1},
#                                 "content": [{"type": "paragraph", "content": [{"type": "text", "text": "3"}]}],
#                             },
#                         ],
#                     },
#                     {
#                         "type": "table_row",
#                         "content": [
#                             {
#                                 "type": "table_cell",
#                                 "attrs": {"colspan": 1, "rowspan": 1},
#                                 "content": [{"type": "paragraph", "content": [{"type": "text", "text": "A"}]}],
#                             },
#                             {
#                                 "type": "table_cell",
#                                 "attrs": {"colspan": 1, "rowspan": 1},
#                                 "content": [{"type": "paragraph", "content": [{"type": "text", "text": "B"}]}],
#                             },
#                             {
#                                 "type": "table_cell",
#                                 "attrs": {"colspan": 1, "rowspan": 1},
#                                 "content": [{"type": "paragraph", "content": [{"type": "text", "text": "C"}]}],
#                             },
#                         ],
#                     },
#                 ],
#             },
#         ],
#     }
#     figure_table_latex = figure_table(figure_table_json)
#     test_figure_table_latex = "\\begin{table}[ht]\n"
#     test_figure_table_latex += "\\centering\n"
#     test_figure_table_latex += "\\caption{Tabular data} \\label{83aa4fee-6879-439b-8601-74e25bb3026e}\n"
#     test_figure_table_latex += "\\begin{tabular}[t]{lll}\n"
#     test_figure_table_latex += "\\toprule\n"
#     test_figure_table_latex += "1 & 2 & 3 \\\\" + "\n"
#     test_figure_table_latex += "A & B & C \\\\" + "\n"
#     test_figure_table_latex += "\\bottomrule\n"
#     test_figure_table_latex += "\\end{tabular}\n"
#     test_figure_table_latex += "\\end{table}"
#     assert test_figure_table_latex == figure_table_latex


def test_abstract():
    abstract_json = {
        "type": "abstract",
        "content": [{"text": "Abstract of the paper", "type": "text"}],
    }
    abstract_latex = abstract(abstract_json)
    test_abstract_latex = "\\begin{abstract}\nAbstract of the paper\n\\end{abstract}"
    assert test_abstract_latex == abstract_latex


def test_json2latex():
    with open(os.path.join(current_directory, "paper.json")) as file_json:
        paper_json = json.load(file_json)
        paper_latex, _, _ = json2latex(paper_json)
    test_paper_latex = Path(os.path.join(current_directory, "paper.tex")).read_text()

    assert test_paper_latex == paper_latex
