import json

from refereed.transform import json2latex

with open("./tests/refereed/transform/content.json") as fp:
    paper_json = json.load(fp)

json2latex(paper_json)
