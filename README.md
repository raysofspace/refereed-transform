# Refereed-transform

Python library for converting different Refereed article formats into each other.

## Development

Clone the repository
```
git clone git@bitbucket.org:raysofspace/refereed-transform.git
```

Create virtual environment and install the package in development mode
```
cd refereed-transform
python3 -m venv .env
source .env/bin/activate
pip install -e .
```

Install the framewok for running tests
```
pip install pytest
```

Run the tests
```
pytest
```
