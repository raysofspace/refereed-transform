from enum import Enum
from functools import reduce
from itertools import chain, product
from string import ascii_lowercase as ALC

import bibtexparser
from bibtexparser.bibdatabase import BibDatabase
from RAKE import NLTKStopList, Rake


class BiblatexBackend(Enum):
    BIBER = 1
    BIBTEX = 2


_REFERENCES = {}
_CITATIONS = {}
_BIBTEX = BibDatabase()
_ASSETS = []
_NUMCHARS = []
for chars in chain(ALC, product(ALC, repeat=2)):
    _NUMCHARS.append("".join(chars))


def required_arguments(arguments):
    latex = ""
    if arguments is not None:
        arguments = [arguments] if isinstance(arguments, str) else arguments
        for arg in arguments:
            latex += "{{{}}}".format(arg)
    return latex


def optional_arguments(options):
    latex = ""
    if options is not None:
        options = [options] if isinstance(options, str) else options
        for opt in options:
            latex += "[{}]".format(opt)
    return latex


def command(command, arguments=None, options=None):
    latex = "\\{}".format(command)
    latex += optional_arguments(options)
    latex += required_arguments(arguments)
    return latex


def begin(environment, arguments=None, options=None):
    latex = command("begin", environment)
    latex += optional_arguments(options)
    latex += required_arguments(arguments)
    return latex


def end(environment, arguments=None, options=None):
    latex = command("end", environment)
    latex += optional_arguments(options)
    latex += required_arguments(arguments)
    return latex


def space():
    return " "


def newline():
    return "\n"


def linebreak():
    return "\\\\"


def get_references(node):
    references = {}
    if "content" in node and len(node["content"]) > 0:
        index_section = index_subsection = index_code = index_equation = index_image = index_table = 0
        rake = Rake(NLTKStopList())
        for subnode in node["content"]:
            if subnode["type"] == "section":
                index_section += 1
                index_subsection = 0
                caption = inline(subnode)
                phrases = rake.run(caption.replace("\\&", ""))
                label = ""
                if len(phrases) > 0:
                    label = phrases[0][0].title().replace(" ", "")
                references[subnode["attrs"]["data-id"]] = "sec:{}:{}".format(index_section, label)
            elif subnode["type"] == "subsection":
                index_subsection += 1
                caption = inline(subnode)
                phrases = rake.run(caption.replace("\\&", ""))
                label = ""
                if len(phrases) > 0:
                    label = phrases[0][0].title().replace(" ", "")
                references[subnode["attrs"]["data-id"]] = "sec:{}.{}:{}".format(index_section, index_subsection, label)
            elif (
                subnode["type"] == "figure_code"
                and "content" in subnode
                and len(subnode["content"]) == 2
                and subnode["content"][0]["type"] == "figure_caption"
                and subnode["content"][1]["type"] == "code_block"
            ):
                index_code += 1
                caption = inline(subnode["content"][0])
                phrases = rake.run(caption.replace("\\&", ""))
                label = ""
                if len(phrases) > 0:
                    label = phrases[0][0].title().replace(" ", "")
                references[subnode["attrs"]["data-id"]] = "lst:{}:{}".format(index_code, label)
            elif (
                subnode["type"] == "figure_equation"
                and "content" in subnode
                and len(subnode["content"]) == 1
                and subnode["content"][0]["type"] == "latex_block"
            ):
                index_equation += 1
                references[subnode["attrs"]["data-id"]] = "eq:{}:{}".format(
                    index_equation, subnode["content"][0]["attrs"]["data-latex"].replace("\\", "")
                )
            elif (
                subnode["type"] == "figure_image"
                and "content" in subnode
                and len(subnode["content"]) == 2
                and subnode["content"][0]["type"] == "image"
                and subnode["content"][1]["type"] == "figure_caption"
            ):
                index_image += 1
                caption = inline(subnode["content"][1])
                phrases = rake.run(caption.replace("\\&", ""))
                label = ""
                if len(phrases) > 0:
                    label = phrases[0][0].title().replace(" ", "")
                references[subnode["attrs"]["data-id"]] = "fig:{}:{}".format(index_image, label)
            elif (
                subnode["type"] == "figure_table"
                and "content" in subnode
                and len(subnode["content"]) == 2
                and subnode["content"][0]["type"] == "figure_caption"
                and subnode["content"][1]["type"] == "table"
            ):
                index_table += 1
                caption = inline(subnode["content"][0])
                phrases = rake.run(caption.replace("\\&", ""))
                label = ""
                if len(phrases) > 0:
                    label = phrases[0][0].title().replace(" ", "")
                references[subnode["attrs"]["data-id"]] = "tbl:{}:{}".format(index_table, label)
    return references


def get_reference(data_id):
    if data_id in _REFERENCES:
        return _REFERENCES[data_id]
    return ""


def reference(node):
    if node["type"] == "reference":
        return command("ref", get_reference(node["attrs"]["data-id"]))
    return ""


def get_citations(node):
    if "content" in node:
        citations = {}
        bibtex = BibDatabase()
        citations_node = next((subnode for subnode in node["content"] if subnode["type"] == "citations"), None)
        if citations_node is not None:
            for citation in citations_node["attrs"]["citations"]:
                key = citation["data-authorship"].split(" and ")[0].split(",")[0].replace(".", "").replace(
                    " ", ""
                ) + str(citation["data-year"])

                if key in bibtex.entries_dict:
                    entry = next((entry for entry in bibtex.entries if entry["ID"] == key), None)
                    if entry is not None:
                        entry["ID"] += _NUMCHARS[0]
                    key += _NUMCHARS[1]
                elif key + _NUMCHARS[0] in bibtex.entries_dict:
                    index = 1
                    while key + _NUMCHARS[index] in bibtex.entries_dict:
                        index += 1
                    key += _NUMCHARS[index]

                bibtex.entries.append(
                    {
                        "ID": key,
                        "title": citation["data-title"],
                        "author": " ".join(citation["data-authorship"].replace(",", " and ").split()),
                        "howpublished": citation["data-publication"],
                        "year": str(format(citation["data-year"])),
                        "ENTRYTYPE": "misc",
                    }
                )
                if citation["data-doi"]:
                    bibtex.entries[-1]["doi"] = citation["data-doi"]
                elif citation["data-url"]:
                    bibtex.entries[-1]["url"] = citation["data-url"]

                citations[citation["data-id"]] = key
    return citations, bibtex


def get_citation(data_id):
    if data_id in _CITATIONS:
        return _CITATIONS[data_id]
    return ""


def citation(node):
    if node["type"] == "citation":
        return command("cite", get_citation(node["attrs"]["data-id"]))
    return ""


def text(node):
    latex = ""
    if node["type"] == "text":
        latex = node["text"].replace("&", "\\&")
        if "marks" in node:
            for mark in node["marks"]:
                if mark["type"] == "bold":
                    latex = command("textbf", latex)
                elif mark["type"] == "italic":
                    latex = command("textit", latex)
    return latex


def latex_inline(node):
    if node["type"] == "latex_inline":
        return "${}$".format(node["attrs"]["data-latex"])
    return ""


def figure_equation(node):
    latex = ""
    if node["type"] == "figure_equation" and "content" in node:
        latex += begin("equation")
        latex += space()
        latex += command("label", get_reference(node["attrs"]["data-id"]))
        latex += newline()
        for subnode in node["content"]:
            if subnode["type"] == "latex_block":
                latex += subnode["attrs"]["data-latex"]
                latex += newline()
        latex += end("equation")
    return latex


def abstract(node):
    latex = ""
    if node["type"] == "abstract":
        latex += begin("abstract")
        latex += newline()
        latex += inline(node)
        latex += newline()
        latex += end("abstract")
    return latex


def inline(node):
    latex = ""
    if "content" in node:
        for subnode in node["content"]:
            if subnode["type"] == "text":
                latex += text(subnode)
            elif subnode["type"] == "latex_inline":
                latex += latex_inline(subnode)
            elif subnode["type"] == "reference":
                latex += reference(subnode)
            elif subnode["type"] == "citation":
                latex += citation(subnode)
    return latex


def figure_table(node):
    latex = ""
    if node["type"] == "figure_table" and "content" in node:
        latex += begin("table", options="ht")
        latex += newline()
        latex += command("centering")
        latex += newline()
        for subnode in node["content"]:
            if subnode["type"] == "figure_caption":
                latex += command("caption", inline(subnode))
                latex += space()
                latex += command("label", get_reference(node["attrs"]["data-id"]))
                latex += newline()
            elif subnode["type"] == "table":
                latex += table(subnode)
        latex += end("tabular")
        latex += newline()
        latex += end("table")
    return latex


def table(node):
    latex = ""
    if node["type"] == "table" and "content" in node:
        num_cols = reduce(lambda x, y: x + y, [col["attrs"]["colspan"] for col in node["content"][0]["content"]])
        latex += begin("tabular", "l" * num_cols, "t")
        latex += newline()
        latex += command("toprule")
        latex += newline()
        prev_rowspans = [1] * num_cols
        for row in node["content"]:
            table_row_latex, prev_rowspans = table_row(row, prev_rowspans)
            latex += table_row_latex
            latex += space()
            latex += linebreak()
            latex += newline()
        latex += command("bottomrule")
        latex += newline()
    return latex


def table_row(node, prev_rowspans):
    latex = ""
    if node["type"] == "table_row" and "content" in node:
        cells = []
        k = 0
        prev_colspan = 1
        for i in range(len(prev_rowspans)):
            if prev_rowspans[i] == 1:
                if prev_colspan == 1:
                    cells.append(table_cell(node["content"][k]))
                    prev_rowspans[i] = node["content"][k]["attrs"]["rowspan"]
                    prev_colspan = node["content"][k]["attrs"]["colspan"]
                    k += 1
                else:
                    prev_rowspans[i] = prev_rowspans[i - 1]
                    prev_colspan -= 1
            else:
                cells.append("")
                prev_rowspans[i] -= 1
        latex = " & ".join(cells)
    return latex, prev_rowspans


def table_cell(node):
    latex = ""
    if node["type"] == "table_cell" and "content" in node:
        paragraphs = []
        for subnode in node["content"]:
            if subnode["type"] == "paragraph" and "content" in subnode:
                paragraphs.append(inline(subnode))
        if len(paragraphs) == 1:
            latex = paragraphs[0]
        elif len(paragraphs) > 1:
            latex = command("makecell", (space() + linebreak() + space()).join(paragraphs))
        if node["attrs"]["colspan"] > 1:
            latex = command("multicolumn", [node["attrs"]["colspan"], "l", latex])
        if node["attrs"]["rowspan"] > 1:
            latex = command("multirow", [node["attrs"]["rowspan"], "*", latex])
    return latex


def figure_code(node):
    latex = ""
    if (
        node["type"] == "figure_code"
        and "content" in node
        and len(node["content"]) == 2
        and node["content"][0]["type"] == "figure_caption"
        and node["content"][1]["type"] == "code_block"
    ):
        latex += begin(
            "lstlisting",
            options=",".join(
                [
                    "caption={}".format(inline(node["content"][0])),
                    "label={}".format(get_reference(node["attrs"]["data-id"])),
                ]
            ),
        )
        latex += newline()
        if "content" in node["content"][1]:
            for subnode in node["content"][1]["content"]:
                if subnode["type"] == "text":
                    latex += subnode["text"]
        latex += newline()
        latex += end("lstlisting")
    return latex


def figure_image(node):
    latex = ""
    if node["type"] == "figure_image" and "content" in node:
        global _ASSETS
        latex += begin("figure", options="ht")
        latex += newline()
        latex += command("centering")
        latex += newline()
        for subnode in node["content"]:
            if subnode["type"] == "image":
                asset_path = "assets/{}".format(subnode["attrs"]["src"].split("/")[-1])
                _ASSETS.append(asset_path)
                if subnode["attrs"]["width"] == "100%":
                    scale = "1.0"
                elif subnode["attrs"]["width"] == "75%":
                    scale = "0.75"
                elif subnode["attrs"]["width"] == "50%":
                    scale = "0.5"
                else:
                    scale = "1.0"
                latex += command(
                    "includegraphics",
                    asset_path,
                    f"width={scale}\\textwidth,height=0.9\\textheight,keepaspectratio",
                )
                latex += newline()
            elif subnode["type"] == "figure_caption":
                latex += command("caption", inline(subnode))
                latex += newline()
        latex += command("label", get_reference(node["attrs"]["data-id"]))
        latex += newline()
        latex += end("figure")
    return latex


def item_list(node):
    latex = ""
    if node["type"] == "ordered_list":
        environment = "enumerate"
    elif node["type"] == "bullet_list":
        environment = "itemize"
    else:
        return latex
    if "content" in node:
        latex += begin(environment)
        latex += newline()
        for subnode in node["content"]:
            if "content" in subnode:
                latex += command("item") + space()
                for subsubnode in subnode["content"]:
                    latex += inline(subsubnode)
                latex += newline()
        latex += end(environment)
    return latex


def authors(node):
    latex = ""
    if node["type"] == "authors":
        lookup = set()  # a temporary lookup set
        affiliations = [
            edge["node"]["affiliation"]
            for edge in node["attrs"]["authors"]["edges"]
            if edge["node"]["affiliation"] not in lookup and lookup.add(edge["node"]["affiliation"]) is None
        ]
        for edge in node["attrs"]["authors"]["edges"]:
            latex += command("author", edge["node"]["name"], str(affiliations.index(edge["node"]["affiliation"]) + 1))
            latex += newline()
        for index, affiliation in enumerate(affiliations):
            if index > 0:
                latex += newline()
            latex += command("affil", affiliation, str(index + 1))
    return latex


def json2latex(node, biblatex_backend=BiblatexBackend.BIBER):
    global _REFERENCES, _CITATIONS, _BIBTEX
    latex = ""
    if node["type"] == "doc" and "content" in node:
        _REFERENCES = get_references(node)
        _CITATIONS, _BIBTEX = get_citations(node)
        latex += command("documentclass", "scrartcl")
        latex += newline()
        latex += command("usepackage", "lmodern")
        latex += newline()
        latex += command("usepackage", "inputenc", "utf8")
        latex += newline()
        latex += command("usepackage", "fontenc", "T1")
        latex += newline()
        latex += command("usepackage", "booktabs")
        latex += newline()
        latex += command("usepackage", "makecell")
        latex += newline()
        latex += command("usepackage", "multirow")
        latex += newline()
        latex += command("usepackage", "listings")
        latex += newline()
        latex += command("usepackage", "graphicx")
        latex += newline()
        latex += command("usepackage", "amsmath")
        latex += newline()
        latex += command("usepackage", "authblk")
        latex += newline()
        if biblatex_backend == BiblatexBackend.BIBTEX:
            latex += command("usepackage", "biblatex", "backend=bibtex,sorting=none")
        else:
            latex += command("usepackage", "biblatex", "sorting=none")
        latex += newline()
        latex += command("addbibresource", "references.bib")
        latex += newline() * 2
        if node["content"][0]["type"] == "title" and "content" in node["content"][0]:
            for subnode in node["content"][0]["content"]:
                latex += command("title", inline(node["content"][0]))
            latex += newline() * 2
        if node["content"][1]["type"] == "authors":
            latex += authors(node["content"][1])
            latex += newline() * 2
        latex += begin("document")
        latex += newline() * 2
        latex += command("maketitle")
        latex += newline() * 2
        for subnode in node["content"]:
            if subnode["type"] == "abstract":
                latex += abstract(subnode)
            elif subnode["type"] == "section":
                latex += command("section", inline(subnode))
                latex += newline()
                latex += command("label", get_reference(subnode["attrs"]["data-id"]))
            elif subnode["type"] == "subsection":
                latex += command("subsection", inline(subnode))
                latex += newline()
                latex += command("label", get_reference(subnode["attrs"]["data-id"]))
            elif subnode["type"] == "passage":
                latex += inline(subnode)
            elif subnode["type"] == "figure_equation":
                latex += figure_equation(subnode)
            elif subnode["type"] == "figure_table":
                latex += figure_table(subnode)
            elif subnode["type"] == "figure_code":
                latex += figure_code(subnode)
            elif subnode["type"] == "figure_image":
                latex += figure_image(subnode)
            elif subnode["type"] == "ordered_list":
                latex += item_list(subnode)
            elif subnode["type"] == "bullet_list":
                latex += item_list(subnode)
            else:
                continue
            latex += newline() * 2
        latex += command("printbibliography")
        latex += newline() * 2
        latex += end("document")
        latex += newline()
    bibtex = bibtexparser.dumps(_BIBTEX)
    return latex, bibtex, _ASSETS
