import os
from codecs import open

from setuptools import find_packages, setup

version_py = open(os.path.join(os.path.dirname(__file__), "version.py")).read().strip().split("=")[-1].replace('"', "")

setup(
    name="refereed-transform",
    version="{ver}".format(ver=version_py),
    description="Refereed library for transforming different article formats into each other",
    url="https://bitbucket.org/raysofspace/refereed-transform",
    packages=find_packages("src", exclude=["test*"]),
    package_dir={"": "src"},
    install_requires=["python-rake", "bibtexparser"],
)
